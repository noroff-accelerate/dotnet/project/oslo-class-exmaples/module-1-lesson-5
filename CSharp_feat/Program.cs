﻿using System;
using System.Collections.Generic;

namespace CSharp_feat
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Structs display
            // STRUCTS
            /* Structs act like classes but are different.
             * Default behavious is value not reference.
             * Can be compared out the box (if all fields are equal), classes have to implement IComparable.
             * Structs cant be null.
             * Shouldnt use parameterless constructors, cant gaurentee validity.
             *  - But its still fine to do so, just be aware
             * Cannot inherit with a struct, but structs can implement interfaces
             * Used for very simple data structures - like a data transfer object
             *  - Dont worry about properties or methods
             *  - Just store simple fields
             */

            // Struct show
            Coordinate coords = new Coordinate() { x = 1, y = 10, z = 80 };
            Console.WriteLine($"X: {coords.x}, Y: {coords.y}, Z: {coords.z}");
            #endregion

            #region Enums display
            // Create a varibale to store the seaon
            Seasons currentSeaon = Seasons.Summer;
            /* Feels a bit meh, but has some power
             * Can do operators on enums if they are [Flags] and are set to bitwise defaults
             * Dont worry about this, just showing you a cooler use.
             */

            #region enums with [Flags]
            // Can store multiple days using |
            // Store days that I work
            Days nickDays = Days.Monday | Days.Wednesday | Days.Friday;
            //Console.WriteLine(nickDays);
            // Store days that Dewald works
            Days dewaldDays = Days.Tuesday | Days.Wednesday | Days.Saturday;
            //Console.WriteLine(dewaldDays);
            // Find out days we are both working
            Days overlaps = nickDays & dewaldDays;
            //Console.WriteLine(overlaps);
            #endregion

            #endregion

            #region Indexers display
            // Declare a quiver, use some arrows, check how many are left
            Quiver rabin = new Quiver();
            rabin[0] = 0;
            rabin[1] = 0;
            rabin[2] = 0;

            Console.WriteLine(rabin.ArrowsLeft());
            #endregion

            #region Delegates display
            // Create some variables that are delegates to store methods
            // The IsNegative and AboveFifty methods are treated as variables
            Check negativeCheck = IsNegative;
            Check aboveFify = AboveFifty;
            // These methods are stored to be used later

            // It is now later - make a list to do some checks on
            List<int> numbers = new List<int>() { 1, -4, 50, 68, 4, 1, -56, 46, -35, 27 };

            // We can now pass the method through as a parameter
            List<int> negativeNumbers = CustomFilter(numbers, IsNegative);
            List<int> bigNumbers = CustomFilter(numbers, AboveFifty);

            #endregion
        }

        #region Structs 
        // Create simple x,y,z coordinate - show cant inherit, but can implement
        public struct Coordinate
        {
            public int x;
            public int y;
            public int z;
        }
        #endregion

        #region Enums
        // Create enum for seasons - yay originality
        // Show default number values, then change monday and see how the rest change
        // Leave it at default
        enum Seasons
        {
            Summer,
            Autumn,
            Winter,
            Spring
        }

        #region [Flags]
        [Flags]
        enum Days
        {
            Monday = 1,
            Tuesday = 2,
            Wednesday = 4,
            Thursday = 8,
            Friday = 16,
            Saturday = 32,
            Sunday = 64
        }
        #endregion

        #endregion

        #region Indexer
        // Create quiver without indexer, then add it to show you can hide the arrows array
        public class Quiver
        {
            int[] arrows = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

            public int this[int i]
            {
                get => arrows[i];
                set => arrows[i] = value;
            }

            public int ArrowsLeft()
            {
                int total = 0;

                foreach (int arrow in arrows)
                    total += arrow;

                return total;
            }
        }

        #endregion

        #region Delegates
        // Declare a delegate to do some check - returning bool, taking in int.
        public delegate bool Check(int number);
        // Create some methods that can be assigned to this delegate, i.e matching its signature
        // IsNegative and Above 50
        // Static doesnt impact the ability to match signatures
        // Either of these methods can be passed into another method as a delegate of type Check
        public static bool IsNegative(int number)
        {
            if (number < 0)
                return true;
            else
                return false;
        }
        public static bool AboveFifty(int number)
        {
            if (number > 50)
                return true;
            else
                return false;
        }

        // Make filter method that can use the delegate as a parameter
        public static List<int> CustomFilter(List<int> numbers, Check customCheck)
        {
            List<int> filteredList = new List<int>();
            foreach(int number in numbers)
            {
                if (customCheck(number))
                {
                    filteredList.Add(number);
                }
            }
            return filteredList;
        }
        #endregion
    }
}
