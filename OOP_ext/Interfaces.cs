﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_ext
{
    // Create some interfaces for behaviour assignment:
    // Peeling, perishing, juicing, grinding, roasting
    public interface IPeelable
    {
        void Peel();
    }

    public interface IPerishable
    {
        void Perish();
    }

    public interface IJuiceable
    {
        void Juice();
    }

    public interface IGrindable
    {
        void Grind();
    }

    public interface IRoastable
    {
        void Roast();
    }
}
