﻿using System;

namespace OOP_ext
{
    class Program
    {
        static void Main(string[] args)
        {
            // Make some food, 1 of each type.
            Almond al = new Almond();
            Pistachio pist = new Pistachio();
            Bannana ban = new Bannana();
            Apple appl = new Apple();
            Carrot car = new Carrot();
            Potato pot = new Potato();
            // Call methods to execute the various behaviours, using polymorphism.
            // Using params
            PeelMyFood(pist,ban,car,pot);
            GrindMyFood(al);
            Expire(ban,appl,pot);
            RoastAndToast(al,pist,pot);
            JuiceMeUpScotty(appl,car);
        }
        #region Method definitions
        // Method to peel any object that can be peeled.
        public static void PeelMyFood(params IPeelable[] foodList)
        {
            foreach(IPeelable food in foodList)
                food.Peel();
        }
        // Method to grind any object that can be grinded
        public static void GrindMyFood(params IGrindable[] foodList)
        {
            foreach(IGrindable food in foodList)
                food.Grind();
        }

        // Method to make anything that can expire, expire
        public static void Expire(params IPerishable[] foodList)
        {
            foreach(IPerishable food in foodList)
                food.Perish();
        }

        // Method to roast anything that can be roasted
        public static void RoastAndToast(params IRoastable[] foodList)
        {
            foreach(IRoastable food in foodList)
                food.Roast();
        }

        // Method to juice anything
        public static void JuiceMeUpScotty(params IJuiceable[] foodList)
        {
            foreach(IJuiceable food in foodList)
                food.Juice();
        }
        #endregion
    }
}
