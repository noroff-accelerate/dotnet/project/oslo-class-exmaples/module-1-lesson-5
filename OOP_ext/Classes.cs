﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_ext
{
    // Create abstract parent classes for nuts, fruits, and vegetables
    #region Abstract parent classes
    public abstract class Nut { }
    public abstract class Fruit { }
    public abstract class Vegetable { }
    #endregion

    // Create child classes that also implement some behaviours
    // Nuts: Almond and Pistachio
    // Fruits: Bannana and Apple
    // Vegetables: Carrot and Potato
    #region Nuts
    public class Almond : Nut, IRoastable, IGrindable
    {
        public void Grind()
        {
            Console.WriteLine("The almonds are ground into a fine powder.");
        }

        public void Roast()
        {
            Console.WriteLine("Roasty toasy almonds.");
        }
    }
    public class Pistachio : Nut, IPeelable, IRoastable
    {
        public void Peel()
        {
            Console.WriteLine("Your fingers break as you peel back the inpenetrable shell.");
        }

        public void Roast()
        {
            Console.WriteLine("Into the furnace they go, when they roast, no one knows.");
        }
    }
    #endregion

    #region Fruits
    public class Bannana : Fruit, IPeelable, IPerishable
    {
        public void Peel()
        {
            Console.WriteLine("The bannana skin is peeled back in the most awkward fashion.");
        }

        public void Perish()
        {
            Console.WriteLine("You open the cupboard excitedly to be greated by a black mushy mess.");
        }
    }

    public class Apple : Fruit, IJuiceable, IPerishable
    {
        public void Juice()
        {
            Console.WriteLine("Remember to remove the stick before you joink the apple in the blender.");
        }

        public void Perish()
        {
            Console.WriteLine("The apple becomes one with the mould and turns into Yoda.");
        }
    }
    #endregion

    #region Vegetables
    public class Carrot : Vegetable, IPeelable, IJuiceable, IRoastable
    {
        public void Juice()
        {
            Console.WriteLine("The carrot is thrust into the blades and becomes Vitamin A water.");
        }

        public void Peel()
        {
            Console.WriteLine("Try not cut your finger off peeling the carrot.");
        }

        public void Roast()
        {
            Console.WriteLine("Slice up the carrot into coins, then throw them like a frisby into the oven.");
        }
    }

    public class Potato : Vegetable, IPeelable, IRoastable, IPerishable
    {
        public void Peel()
        {
            Console.WriteLine("Remove the nasty earth from the potato with a broadsword.");
        }

        public void Perish()
        {
            Console.WriteLine("Some form of tree grows out of the potato accompanied by a horrible smell.");
        }

        public void Roast()
        {
            Console.WriteLine("Peel it, boild it, oil it, scuff the surface, put it in the oven and recieve the roasted gift from the gods.");
        }
    }
    #endregion

}
